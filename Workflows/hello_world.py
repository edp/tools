# Copied from https://github.com/couler-proj/couler/examples/hello_world.py
# just to try changing the namespace (from default to argo)
import couler.argo as couler
from couler.argo_submitter import ArgoSubmitter

couler.run_container(
    image="docker/whalesay", command=["cowsay"], args=["hello world"]
)

submitter = ArgoSubmitter(namespace='argo')
result = couler.run(submitter=submitter)
