## To be evaluated
 - [ArgoProj](https://github.com/argoproj)
   * Pros: directory structured inputs/outpus
   * [DAGs](https://en.wikipedia.org/wiki/Directed_acyclic_graph) workflows
   * ArgoWorkflows is a container-native workflow engine for orchestrating jobs on Kubernetes
     - Salient [Features for NumExp](https://github.com/argoproj/argo-workflows#features)
       * Open source
       * Python SDK: [Couler](https://github.com/couler-proj/couler) (that "aims to provide a 
         unified interface for constructing and managing workflows on different workflow 
         engines, such as Argo Workflows, Tekton Pipelines, and Apache Airflow)
       * CLI ("argo submit myworkflow.yaml")
       * DAG or steps based declaration of workflows with: loops, parameterization, conditionals, timeouts, exit hooks
       * Worflow templates: workflows persisted on the cluster.
       * Robustness: Retry, Resubmit (memoized), Suspend & Resume, Cancellation
       * Native Artifact support (S3, Artifactory, Alibaba Cloud OSS, HTTP, Git, GCS, raw)
       * K8s resource orchestration (Volumes)
     - Authentication: single sign-in with github account, client authentication ("argo auth authentication")
     - Usages: academia/industry, ML, ETL, batch/data processing, CI/CD
     - [Demo environment](https://workflows.apps.argoproj.io/workflows/argo) (online)
  
 - [OGC API - Processes](https://ogcapi.ogc.org/processes/): the :OGC API - Processes" specification defines 
   * how a client application can request the execution of a process, 
   * how the inputs to that process can be provided, and 
   * how the output from the process is handled. 
   
   The specification allows for the wrapping of computational tasks into an executable process that can be invoked by a client applicatio)
